-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 25 jan. 2018 à 02:07
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `oc_expressfood`
--

-- --------------------------------------------------------

--
-- Structure de la table `adress`
--

DROP TABLE IF EXISTS `adress`;
CREATE TABLE IF NOT EXISTS `adress` (
  `adressID` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `zipCode` int(11) NOT NULL,
  `longitude` float NOT NULL,
  `latitude` float NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`adressID`),
  KEY `FK_Adress_userID` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `categoryID` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`categoryID`),
  KEY `label` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `creditcard`
--

DROP TABLE IF EXISTS `creditcard`;
CREATE TABLE IF NOT EXISTS `creditcard` (
  `numero` bigint(20) NOT NULL,
  `cardholderName` varchar(255) NOT NULL,
  `expiryDate` date NOT NULL,
  `cryptogram` int(11) NOT NULL,
  PRIMARY KEY (`numero`),
  KEY `cardholderName` (`cardholderName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `deliveryman`
--

DROP TABLE IF EXISTS `deliveryman`;
CREATE TABLE IF NOT EXISTS `deliveryman` (
  `dmID` int(11) NOT NULL AUTO_INCREMENT,
  `longitude` float NOT NULL,
  `latitude` float NOT NULL,
  `userID` int(11) NOT NULL,
  `status` varchar(25) NOT NULL,
  PRIMARY KEY (`dmID`,`userID`),
  KEY `FK_Deliveryman_userID` (`userID`),
  KEY `FK_Deliveryman_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `dish`
--

DROP TABLE IF EXISTS `dish`;
CREATE TABLE IF NOT EXISTS `dish` (
  `dishID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `creationDate` date NOT NULL,
  `picture` text,
  `categoryID` int(11) NOT NULL,
  PRIMARY KEY (`dishID`),
  KEY `name` (`name`),
  KEY `FK_Dish_categoryID` (`categoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `dish_deliveryman`
--

DROP TABLE IF EXISTS `dish_deliveryman`;
CREATE TABLE IF NOT EXISTS `dish_deliveryman` (
  `quantity` int(11) NOT NULL,
  `dmID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `dishID` int(11) NOT NULL,
  PRIMARY KEY (`dmID`,`userID`,`dishID`),
  KEY `FK_DISH_DELIVERYMAN_userID` (`userID`),
  KEY `FK_DISH_DELIVERYMAN_dishID` (`dishID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `dish_ingredient`
--

DROP TABLE IF EXISTS `dish_ingredient`;
CREATE TABLE IF NOT EXISTS `dish_ingredient` (
  `dishID` int(11) NOT NULL,
  `ingredientID` int(11) NOT NULL,
  PRIMARY KEY (`dishID`,`ingredientID`),
  KEY `FK_DISH_INGREDIENT_ingredientID` (`ingredientID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `history`
--

DROP TABLE IF EXISTS `history`;
CREATE TABLE IF NOT EXISTS `history` (
  `historyDate` date NOT NULL,
  `activationStatus` tinyint(1) NOT NULL,
  `dishID` int(11) NOT NULL,
  PRIMARY KEY (`dishID`),
  KEY `activationStatus` (`activationStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `ingredient`
--

DROP TABLE IF EXISTS `ingredient`;
CREATE TABLE IF NOT EXISTS `ingredient` (
  `ingredientID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`ingredientID`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `orderID` int(11) NOT NULL AUTO_INCREMENT,
  `creationDate` date NOT NULL,
  `dmID` int(11) DEFAULT NULL,
  `userID` int(11) NOT NULL,
  `status` varchar(25) NOT NULL,
  PRIMARY KEY (`orderID`),
  KEY `FK_Orders_dmID` (`dmID`),
  KEY `FK_Orders_userID` (`userID`),
  KEY `FK_Orders_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `orders_user_adress`
--

DROP TABLE IF EXISTS `orders_user_adress`;
CREATE TABLE IF NOT EXISTS `orders_user_adress` (
  `userID` int(11) NOT NULL,
  `orderID` int(11) NOT NULL,
  `adressID` int(11) NOT NULL,
  PRIMARY KEY (`userID`,`orderID`,`adressID`),
  KEY `FK_ORDERS_USER_ADRESS_orderID` (`orderID`),
  KEY `FK_ORDERS_USER_ADRESS_adressID` (`adressID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `order_dish`
--

DROP TABLE IF EXISTS `order_dish`;
CREATE TABLE IF NOT EXISTS `order_dish` (
  `quantity` int(11) NOT NULL,
  `orderID` int(11) NOT NULL,
  `dishID` int(11) NOT NULL,
  PRIMARY KEY (`orderID`,`dishID`),
  KEY `FK_ORDER_DISH_dishID` (`dishID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `roleID` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`roleID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `status`
--

DROP TABLE IF EXISTS `status`;
CREATE TABLE IF NOT EXISTS `status` (
  `status` varchar(25) NOT NULL,
  PRIMARY KEY (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `creationDate` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `roleID` int(11) NOT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `email` (`email`),
  KEY `FK_Users_roleID` (`roleID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user_creditcard`
--

DROP TABLE IF EXISTS `user_creditcard`;
CREATE TABLE IF NOT EXISTS `user_creditcard` (
  `numero` bigint(20) NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`numero`,`userID`),
  KEY `FK_USER_CREDITCARD_userID` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `adress`
--
ALTER TABLE `adress`
  ADD CONSTRAINT `FK_Adress_userID` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);

--
-- Contraintes pour la table `deliveryman`
--
ALTER TABLE `deliveryman`
  ADD CONSTRAINT `FK_Deliveryman_status` FOREIGN KEY (`status`) REFERENCES `status` (`status`),
  ADD CONSTRAINT `FK_Deliveryman_userID` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);

--
-- Contraintes pour la table `dish`
--
ALTER TABLE `dish`
  ADD CONSTRAINT `FK_Dish_categoryID` FOREIGN KEY (`categoryID`) REFERENCES `category` (`categoryID`);

--
-- Contraintes pour la table `dish_deliveryman`
--
ALTER TABLE `dish_deliveryman`
  ADD CONSTRAINT `FK_DISH_DELIVERYMAN_dishID` FOREIGN KEY (`dishID`) REFERENCES `dish` (`dishID`),
  ADD CONSTRAINT `FK_DISH_DELIVERYMAN_dmID` FOREIGN KEY (`dmID`) REFERENCES `deliveryman` (`dmID`),
  ADD CONSTRAINT `FK_DISH_DELIVERYMAN_userID` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);

--
-- Contraintes pour la table `dish_ingredient`
--
ALTER TABLE `dish_ingredient`
  ADD CONSTRAINT `FK_DISH_INGREDIENT_dishID` FOREIGN KEY (`dishID`) REFERENCES `dish` (`dishID`),
  ADD CONSTRAINT `FK_DISH_INGREDIENT_ingredientID` FOREIGN KEY (`ingredientID`) REFERENCES `ingredient` (`ingredientID`);

--
-- Contraintes pour la table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `FK_History_dishID` FOREIGN KEY (`dishID`) REFERENCES `dish` (`dishID`);

--
-- Contraintes pour la table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FK_Orders_dmID` FOREIGN KEY (`dmID`) REFERENCES `deliveryman` (`dmID`),
  ADD CONSTRAINT `FK_Orders_status` FOREIGN KEY (`status`) REFERENCES `status` (`status`),
  ADD CONSTRAINT `FK_Orders_userID` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);

--
-- Contraintes pour la table `orders_user_adress`
--
ALTER TABLE `orders_user_adress`
  ADD CONSTRAINT `FK_ORDERS_USER_ADRESS_adressID` FOREIGN KEY (`adressID`) REFERENCES `adress` (`adressID`),
  ADD CONSTRAINT `FK_ORDERS_USER_ADRESS_orderID` FOREIGN KEY (`orderID`) REFERENCES `orders` (`orderID`),
  ADD CONSTRAINT `FK_ORDERS_USER_ADRESS_userID` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);

--
-- Contraintes pour la table `order_dish`
--
ALTER TABLE `order_dish`
  ADD CONSTRAINT `FK_ORDER_DISH_dishID` FOREIGN KEY (`dishID`) REFERENCES `dish` (`dishID`),
  ADD CONSTRAINT `FK_ORDER_DISH_orderID` FOREIGN KEY (`orderID`) REFERENCES `orders` (`orderID`);

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_Users_roleID` FOREIGN KEY (`roleID`) REFERENCES `role` (`roleID`);

--
-- Contraintes pour la table `user_creditcard`
--
ALTER TABLE `user_creditcard`
  ADD CONSTRAINT `FK_USER_CREDITCARD_numero` FOREIGN KEY (`numero`) REFERENCES `creditcard` (`numero`),
  ADD CONSTRAINT `FK_USER_CREDITCARD_userID` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
